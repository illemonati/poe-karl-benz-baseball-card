import React from "react";
import "./App.css";

import karlBenzImg from "./assets/Carl_Benz.png";
import automobileImg from "./assets/1885Benz.jpg";

const info = {
    name: "Karl Friedrich Benz",
    img: karlBenzImg,
};

function App() {
    return (
        <div className="App">
            <div className="card-main">
                <div className="head-image-container">
                    <img className="head-image" src={info.img} alt="" />
                </div>
                <div className="title-text">
                    <h1>{info.name}</h1>
                </div>
                <div className="education">
                    <h2> Education </h2>
                    <h3>Karlsruhe Institute of Technology</h3>
                    <h4>Studied Mechnical Engineering</h4>
                    <h4>Admitted at age 15</h4>
                    <h4>Graduated at age 19</h4>
                </div>
                <div className="point-of-interest">
                    <h2>Point of Interest</h2>
                    <h4>His will to improve his own condition.</h4>
                    <h4>
                        He was born poor, raised by a single mother after his
                        father died when he was 2
                    </h4>
                    <h4>
                        Yet he went to college at 15, graduated at 19, invented
                        the first automobile, then formed what will become one
                        of the largest auto companies in the world
                    </h4>
                </div>
                <div className="accomplishments">
                    <h2>Accomplishments</h2>
                    <h4>
                        Karl Benz invented many of the parts neccery for an
                        automobile, as well as created the world's first
                        automobile
                    </h4>
                    <h4>
                        Patented the spark plug, the carburetor, the clutch, the
                        gear shift, and the water radiator
                    </h4>
                    <h4>Created a petrol based two stroke engine</h4>
                    <h4>
                        Put all inventions togather into the Benz Patent
                        Motorwagen, the world's first self contained, gas
                        powered, automobile, which is debated on wether it was a
                        automobile as it was a tricycle
                    </h4>
                    <h4>
                        Created the company that will eventually become Mercedes
                        Benz
                    </h4>
                </div>

                <div className="automobile-image-container">
                    <img
                        className="automobile-image"
                        src={automobileImg}
                        alt=""
                    />
                </div>
            </div>
        </div>
    );
}

export default App;
